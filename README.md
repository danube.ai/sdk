# danube.ai SDK - Examples

This repository shows examples on how to use the services of [danube.ai](https://danube.ai/). Visit the [danube-sdk npm package](https://www.npmjs.com/package/danube-sdk) page to get more information on the interfaces this SDK provides.


## Subscription

Visit https://danube.ai/ if you want to get an API key.


### Setup

All demos require the following setup:

```shell
$ npm install
$ export API_KEY={my_api_key}
[ $ export DANUBE_API_URL={api_url} ]
```

Make sure to replace {my_api_key} with your danube.ai API key. (Note that you require a different API key for the danube.ai prediction and the danube.ai recommendation services.)

Optionally you can also specify an api url by replacing {api_url} and including the 3rd line. This defaults to `https://api.danube.ai/`, if not provided.


### danube.ai prediction

Under `src/danubePrediction/...` you can find examples on how to use the danube.ai prediction API with this SDK (Using `DanubeClient.setRules`, `DanubeClient.getRules` and `DanubeClient.danubePrediction`).

You can run the examples like follows:

```shell
$ node src/danubePrediction/setRules_demo.js
$ node src/danubePrediction/danubePrediction_withRulesId_demo
```

You need to run `setRules_demo` before `danubePrediction_withRulesId_demo` as it requires the created rules-set.

```shell
$ node src/danubePrediction/danubePrediction_withRulesObject_demo.js
```

In `danubePrediction_withRulesObject_demo` the rules are passed directly instead.

```shell
$ node src/danubePrediction/getRules_demo.js
```

With `getRules_demo` you can check your previously created rule-sets with this SDK.


### danube.ai recommendation

Under `src/danubeRecommendation/...` you can find an examples on how to use the danube.ai recommendation API with this SDK (Using `DanubeClient.danubeRecommendation` and `DanubeClient.resetCorrelationMatrix`).

You can run the example like follows:

```shell
$ node src/danubeRecommendation/danubeRecommendation_demo.js
$ node src/danubeRecommendation/danubeRecommendation_readOnly_demo.js
```

With `danubeRecommendation_demo` you can create correlations between the test data and get recommendations after each API call. With `danubeRecommendation_readOnly_demo` you can retrieve recommendations for some input data without creation new correlations as the API endpoint is called read-only here.

```shell
$ node src/danubeRecommendation/resetCorrelationMatrix_demo.js
```

You can reset the correlations between the test data with `resetCorrelationMatrix_demo`.
