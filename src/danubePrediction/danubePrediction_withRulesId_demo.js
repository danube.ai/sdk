const { DanubeClient } = require('danube-sdk');

const danubeClient = new DanubeClient(
  process.env.API_KEY, // specify your api key in the environment (or directly set it here)
  process.env.DANUBE_API_URL, // will fallback to 'https://api.danube.ai/' if null --> won't need to specify this yourself
);

async function runDemo() {
  console.log('');
  console.log('#######################################################');
  console.log('######### starting danube.ai prediction demo ##########');
  console.log('');
  console.log(`Created danube.ai client for API key '${process.env.API_KEY}'.`);
  console.log('');

  // Note: You need to execute the setRules demo first for this rules set to be created.
  const rulesId = 'my-sdk-test-rule-set-1'; // the name of your rule-set to use.

  console.log('------------------ danube prediction ------------------');
  console.log('');

  // create some example test data and stringify it as a json
  const testData = [
    {
      id: 0,
      title: "job-1",
      field: "Test/QA",
      salaryFrom: 36000,
      salaryTo: 50000,
      daysAgo: 240,
      companyType: "Startup",
      jobLevel: "Experienced",
      technologies: ["Python", "Java", "C++", "C"],
      benefits: ["Flexible working hours", "Team events"]
    },
    {
      id: 1,
      title: "job-2",
      field: "Software",
      salaryFrom: 42000,
      salaryTo: 60000,
      daysAgo: 100,
      companyType: "Established company",
      // jobLevel missing --> data may be incomplete
      technologies: ["Git", "Docker", "JavaScript"]
      // benefits missing --> data may be incomplete
    },
  ];
  const stringifiedTestData = JSON.stringify(testData);

  // create some example search data and stringify it as a json
  const testSearchData = {
    companyType: ["Startup"],
    jobLevel: ["Junior", "Experienced"],
    technologies: ["SQL", "Java", "Linux"],
    benefits: ["Flexible working hours", "Home office"]
  };
  const stringifiedTestSearchData = JSON.stringify(testSearchData);

  // define initial scores
  const initialScores = [
    {property: "salaryFrom", score: 1},
    {property: "salaryTo", score: 1},
    {property: "daysAgo", score: 0.5}, // might be weighted less important
    {property: "companyType", score: 1},
    {property: "jobLevel", score: 1},
    {property: "technologies", score: 2}, // might be weighted more important
    {property: "benefits", score: 1},
  ];

  console.log('test data:');
  console.log(testData);
  console.log('');
  console.log('test search data:');
  console.log(testSearchData);
  console.log('');
  console.log('initial scores:');
  console.log(initialScores);
  console.log('\n');
  console.log('requesting danube prediction...');
  console.log('');

  // let danube.ai sort your data
  const results = await danubeClient.danubePrediction(
    rulesId,
    stringifiedTestData,
    stringifiedTestSearchData,
    initialScores,
    'mixed', // strategy
    0.75, // mix-factor
    1, // impact
  );

  console.log('results:');
  console.log(results);

  console.log('');
  console.log('######### danube.ai prediction demo finished ##########');
  console.log('#######################################################');
  console.log('');
}

runDemo();
