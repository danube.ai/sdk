const { DanubeClient } = require('danube-sdk');

const danubeClient = new DanubeClient(
  process.env.API_KEY, // specify your api key in the environment (or directly set it here)
  process.env.DANUBE_API_URL, // will fallback to 'https://api.danube.ai/' if null --> won't need to specify this yourself
);

async function runDemo() {
  console.log('');
  console.log('#######################################################');
  console.log('########## starting danube.ai getRules demo ###########');
  console.log('');
  console.log(`Created danube.ai client for API key '${process.env.API_KEY}'.`);
  console.log('');

  console.log('-------------------- getting rules --------------------');
  console.log('');

  const savedRulesSets = await danubeClient.getRules();

  console.log('retrieved rules-sets:');
  console.log(savedRulesSets);

  console.log('');
  console.log('########## danube.ai getRules demo finished ###########');
  console.log('#######################################################');
  console.log('');
}

runDemo();
