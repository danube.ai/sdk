const { DanubeClient } = require('danube-sdk');

const danubeClient = new DanubeClient(
  process.env.API_KEY, // specify your api key in the environment (or directly set it here)
  process.env.DANUBE_API_URL, // will fallback to 'https://api.danube.ai/' if null --> won't need to specify this yourself
);

async function setRules(rulesId, rules) {
  const override = true; // define if your rule-set will override a rule-set with the same id, if it already exists.

  console.log('requesting saving rules...');
  console.log('');

  // save your rule-set
  const setRulesSuccess = await danubeClient.setRules(
    rulesId,
    override,
    rules,
  );

  console.log('Rules successfully saved:', setRulesSuccess);
  console.log('\n');
}

async function runDemo() {
  console.log('');
  console.log('#######################################################');
  console.log('########## starting danube.ai setRules demo ###########');
  console.log('');
  console.log(`Created danube.ai client for API key '${process.env.API_KEY}'.`);
  console.log('');

  console.log('----------------- setting rules-set 1 -----------------');
  console.log('');

  // Note: This rules set is used for the danube prediction demo
  const rulesId_1 = 'my-sdk-test-rule-set-1';

  // specify example rules-set 1
  const rules_1 = [
    {
      property: 'salaryFrom',
      type: 'PERCENTAGE',
    },
    {
      property: 'salaryTo',
      type: 'PERCENTAGE',
    },
    {
      property: 'daysAgo',
      type: 'INVERSE_PERCENTAGE',
    },
    {
      property: 'companyType',
      type: 'EQUALS',
      equalityScores: [
        {
          value: 'Startup',
          score: 1,
        },
        {
          value: 'Digital agency',
          score: 2,
        },
        {
          value: 'Established company',
          score: 3,
        },
      ],
    },
    {
      property: 'jobLevel',
      type: 'EQUALS',
      equalityScores: [
        {
          value: 'Junior',
          score: 1,
        },
        {
          value: 'Experienced',
          score: 2,
        },
        {
          value: 'Senior',
          score: 3,
        },
        {
          value: 'Lead',
          score: 4,
        },
      ],
    },
    {
      property: 'timeDistance',
      type: 'INVERSE_PERCENTAGE',
    },
    {
      property: 'technologies',
      type: 'OVERLAP_GLOBAL',
    },
    {
      property: 'benefits',
      type: 'OVERLAP_LOCAL',
    },
  ];

  await setRules(rulesId_1, rules_1);

  console.log('----------------- setting rules-set 2 -----------------');
  console.log('');

  const rulesId_2 = 'my-sdk-test-rule-set-2';

  // specify example rules-set 2
  const rules_2 = [
    {
      property: 'p3',
      type: 'PERCENTAGE',
    },
    {
      property: 'p4',
      type: 'EQUALS',
      equalityScores: [
        {
          value: 'v1',
          score: 1,
        },
        {
          value: 'v2',
          score: 2,
        },
      ],
    },
  ];

  await setRules(rulesId_2, rules_2);

  console.log('');
  console.log('########## danube.ai setRules demo finished ###########');
  console.log('#######################################################');
  console.log('');
}

runDemo();
