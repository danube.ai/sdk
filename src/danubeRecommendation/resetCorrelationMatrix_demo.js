const { DanubeClient } = require('danube-sdk');

const danubeClient = new DanubeClient(
  process.env.API_KEY, // specify your api key in the environment (or directly set it here)
  process.env.DANUBE_API_URL, // will fallback to 'https://api.danube.ai/' if null --> won't need to specify this yourself
);

async function runDemo() {
  console.log('');
  console.log('######################################################');
  console.log('### starting danube.ai resetCorrelationMatrix demo ###');
  console.log('');
  console.log(`Created danube.ai client for API key '${process.env.API_KEY}'.`);
  console.log('');

  console.log('requesting resetting correlation matrix...');
  const resetCorrelationMatrixSuccess = await danubeClient.resetCorrelationMatrix();

  console.log('correlation matrix successfully resetted:', resetCorrelationMatrixSuccess);

  console.log('');
  console.log('### danube.ai resetCorrelationMatrix demo finished ####');
  console.log('#######################################################');
  console.log('');
}

runDemo();
