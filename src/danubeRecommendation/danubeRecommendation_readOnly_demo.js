const { DanubeClient } = require('danube-sdk');

const danubeClient = new DanubeClient(
  process.env.API_KEY, // specify your api key in the environment (or directly set it here)
  process.env.DANUBE_API_URL, // will fallback to 'https://api.danube.ai/' if null --> won't need to specify this yourself
);

async function runDemo() {
  console.log('');
  console.log('#######################################################');
  console.log('####### starting danube.ai recommendation demo ########');
  console.log('');
  console.log(`Created danube.ai client for API key '${process.env.API_KEY}'.`);
  console.log('');

  // get recommendation data with read-only call
  const result = await danubeClient.danubeRecommendation(
    JSON.stringify([{ page: 'Home' }]),
    3,
    null,
    false,
  );

  console.log('recommendations:');
  console.log(result);
  console.log('\n');

  console.log('####### danube.ai recommendation demo finished ########');
  console.log('#######################################################');
  console.log('');
}

runDemo();
